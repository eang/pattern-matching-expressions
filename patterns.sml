(* Datatype definitions *)

datatype Val = NUM of int | COUPLE of Val*Val;

datatype Pat = CONST of int | VAR of string | PAIR of Pat*Pat;

datatype Exp = NUMERAL of int | VARR of string | DUO of Exp*Exp | LET of (Pat*Pat*Exp);

(* Given an element x and a list L, remove from L each occurrence of x *)
fun removeAll x [] = [] |
    removeAll x (y::L) = if (x=y) then (removeAll x L)
                         else (y::(removeAll x L));

(* Given a list L, return L without duplicate elements *)
fun removeDup [] = [] |
    removeDup (x::L) = x::(removeAll x (removeDup L));

(* Given a pattern P, return delta(P) *)
fun delta (CONST p) = nil
  | delta (VAR x) = [x]
  | delta (PAIR (p,q)) = removeDup(delta(p)@delta(q));

(* Given an environemnt E, return dom(E) *)
fun domEnv nil = nil
  | domEnv ((x,v)::E) = removeDup([x]@domEnv(E));

exception noSuchVariable of string;

(* Check whether a given key x (i.e. a variable) belongs to the given list of pairs (i.e. an environment). *)
fun find x nil = raise noSuchVariable ("Error: no such variable "^x^" in the environment.\n") |
    find x ((y,v)::L) = if (x=y) then v else (find x L);

(* Check whether a given key x belongs to the given list of pairs. *)
fun member x nil = false |
    member x ((y,v)::L) = (x=y) orelse (member x L);

(* Given a key x (i.e. a variable) and a list of pairs L, remove from L each pair where x is the first element. *)
fun removeElem x L = List.filter (fn (h,v) => h <> x) L;

(* Given an environment E and a list of variables, return the reduction of E by those variables. *)
fun redEnv E nil = E |
    redEnv E (x::L) = if (L <> nil) then (redEnv (removeElem x E) L) else removeElem x E;

(* Convert a given value to a pattern. *)
fun valToPat (NUM x) = (CONST x) |
    valToPat (COUPLE(x,y)) = PAIR(valToPat(x), valToPat(y));

(* Convert a given pattern to a value. *)
fun patToVal (CONST x) = (NUM x) |
    patToVal (PAIR(x,y)) = COUPLE(patToVal(x), patToVal(y));

(* Given an environment E and a pattern P, return E(P). *)
fun patternSub E (CONST x) = CONST x |
    patternSub E (PAIR (x,y)) = PAIR(patternSub E x, patternSub E y) |
    patternSub E (VAR x) = if (member x E) then
                                    (case (find x E) of
                                    (NUM k) => (CONST k) |
                                    COUPLE(x,y) => patternSub E (PAIR (valToPat x, valToPat y)))
                           else (VAR x);

(* Convert a given pattern to a human-readable string. *)
fun readPat (CONST k) = Int.toString k |
    readPat (VAR x) = x |
    readPat (PAIR (x, y)) = "(" ^ (readPat x) ^ ", " ^ (readPat y) ^ ")";

(* Convert a given expression to a human-readable string. *)
fun readExp (NUMERAL k) = Int.toString k |
    readExp (VARR x) = x |
    readExp (DUO (x, y)) = "(" ^ (readExp x) ^ ", " ^ (readExp y) ^ ")" |
    readExp (LET (p,q,x)) = "LET " ^ (readPat p) ^ " = " ^ (readPat q) ^ " in " ^ (readExp x);

(* Convert a given value to a human-readable string. *)
fun readVal (NUM k ) =  Int.toString k |
    readVal (COUPLE (x, y)) = "(" ^ (readVal x) ^ ", " ^ (readVal y) ^ ")";

(* Convert a given system to a human-readable string. *)
fun readSist [] = "" |
    readSist ((x,v)::S) = readPat(x)^" = "^readPat(v)^"; "^readSist(S);

(* Convert a given environment to a human-readable string. *)
fun readEnv [] = "" | 
    readEnv ((x,v)::E) = "("^x^","^readVal(v)^")"^" "^(readEnv E);

(* Convert a given list to a human-readable string. *)
fun readList [] = "" |
    readList (x::L) = "x"^"  "^(readList(L));

fun printExp x = print((readExp x)^"\n");
fun printSist S = print((readSist S)^"\n");

(* Check whether a given element x belongs to the given list L. *)
fun singleMember x nil = false |
    singleMember x (y::L) = (y=x) orelse (singleMember x L);

(* Check whether a given pattern is constant. *)
fun constPair (CONST k)  = true |
    constPair (VAR x) = false | 
    constPair (PAIR(u, v)) = (constPair u) andalso (constPair v);

(* Verifica che A è una sottolista ("sottoinsieme") di B *)
(* Check whether A is a subset (i.e. sublist) of B. *)
fun subSet nil B = true | 
    subSet (s::A) B = (singleMember s B) andalso  (subSet A B);

(* Check whether the set A is equal to the set B. *)
fun equalSet A B = (List.length A)=(List.length B) andalso (subSet A B);

exception error of string;

(* Given two patterns to match, build their system, i.e. a list of pairs (VAR x, P) which denote equations x=P, where P is a pattern. *)
fun sist (VAR x) (CONST k) = [(VAR x, CONST k)] |
    sist (CONST k) (VAR x) = [(VAR x, CONST k)] |
    sist (VAR x) (VAR y) = if (x=y) then [] else [(VAR x, VAR y)] |
    sist (VAR x) (PAIR (v, w)) = [(VAR x, PAIR (v, w))] |
    sist (PAIR (v, w)) (VAR x) =  [(VAR x, PAIR (v, w))] |
    sist (CONST x) (CONST y) =  if (x=y) then [] else raise error ("Error: cannot match "^(readPat(CONST x))^" with "^(readPat(CONST y))^"\n") |
    sist (CONST k) (PAIR (x,y)) = raise error  ("Error: cannot match "^(readPat(CONST k))^" with "^(readPat(PAIR(x,y)))^"\n")  |
    sist (PAIR (x,y)) (CONST k) = raise error ("Error: cannot match "^(readPat(PAIR(x,y)))^" with "^(readPat(CONST k))^"\n")  |
    sist (PAIR (v,w)) (PAIR (x,y)) = (sist v x)@(sist w y);

(* Given an equation x=P (where P is a pattern) and an equation v=k, assign the value k to each occurrence of v either in P or in x itself. *)
fun auxSub (VAR x, VAR y) (VAR v, k) = if(x=v) then (k, VAR y)
                                       else if (y=v) then (VAR x, k)
                                       else (VAR x, VAR y) |
    auxSub (VAR x, PAIR(VAR z, VAR y)) (VAR v, k) = if (z=v) then auxSub (VAR x, PAIR(k, VAR y)) (VAR v, k)
                                                    else if (y=v) then auxSub (VAR x, PAIR(VAR z, k)) (VAR v, k)
                                                    else if (x=v) then  (k, PAIR(VAR z, VAR y)) 
                                                    else (VAR x, PAIR(VAR z, VAR y)) | 
    auxSub (VAR x, PAIR(VAR z, y)) (VAR v, k) = if (z=v) then auxSub (VAR x, PAIR(k, y)) (VAR v, k)
                                                else if (x=v) then  (k, PAIR(VAR z, y)) 
                                                else (VAR x, PAIR(VAR z, y)) |
    auxSub (VAR x, PAIR(z, VAR y)) (VAR v, k) = if (y=v) then auxSub (VAR x, PAIR(z, k)) (VAR v, k)
                                                else if (x=v) then  (k, PAIR(z, VAR y)) 
                                                else (VAR x, PAIR( z, VAR y)) |
    auxSub (VAR x, PAIR(a,b))  (VAR v, k) = if (x=v) then (k, PAIR(a,b)) 
                                            else (VAR x, PAIR(a,b)) |
    auxSub (VAR x, CONST k) (u,v) = (VAR x, CONST k);

(* Given a system S of equations and an equation x=k, assign the value k to each occurrence of x in S. *)
fun substitute [] (VAR x, k) = [] |
    substitute ((CONST h, CONST j)::S) (VAR x, k)= substitute S (VAR x, k) |
    substitute ((VAR v, p)::S) (VAR x, k) = (auxSub (VAR v, p) (VAR x, k))::(substitute S (VAR x, k)) |
    substitute ((p, VAR v)::S) (VAR x, k) = (auxSub (VAR v, p) (VAR x, k))::(substitute S (VAR x, k));

(* Given a variable x, a system S and a constant pattern, check whether at least one variable y=x in S has a different pattern. *)
fun checkConflict x nil h  = false |
    checkConflict x ((y,k)::S) h = if((constPair k) andalso x=y andalso h<>k ) then true else false orelse (checkConflict x S h);

(* Given a system S and its copy E, check whether there are errors in S due to "constants", using E as support. *)
fun constError E nil = false |
    constError E ((x,k)::S) = if(constPair k) then (checkConflict x E k) orelse (constError E S) else (constError E S) ;

(* Given a system S, return the list of variables occurring in equations such as: x=y | x=pair | pair=pair. *)
fun varList nil = [] |
    varList ((VAR x, VAR y)::S) = [x,y]@(varList S) |
    varList (((VAR x, PAIR(u,v)))::S) = (x::delta(PAIR(u,v)))@(varList S) |
    varList ((PAIR(u,v), PAIR(x,y))::S) = (delta(PAIR(u,v)))@(delta(PAIR(x,y)))@(varList S) |
    varList (_::S) = varList(S);

(* Check whether a given variable x is associated to a value in the given system S. *)
fun checkSost x [] = false |
    checkSost x ((VAR y, CONST k)::S) = if (x=y) then true else false orelse (checkSost x S) |
    checkSost x ((VAR y, PAIR(u,v))::S) = if (x=y andalso (constPair (PAIR(u,v)))) then true else false orelse (checkSost x S) |
    checkSost x (_::S) = false orelse (checkSost x S);

(* Check whether there is circularity in the given system S, i.e. whether each variable in the given list has no associated values in S. *)
fun circSafe [] S  = true |   (* Note: this is not the base case. The real one is in the if clause below. *)
    circSafe (x::L) S = if (L=[]) then false orelse (checkSost x S) else (checkSost x S) orelse (circSafe L S);

(* Check whether there is circularity in the given system S. *)
fun circ S = not(circSafe (removeDup(varList S)) S);

exception circularity of string;
exception constantError of string;

(* Given a system S and a copy E of S, solve S and return an environment E' such that P=Q' |- E'. *)
fun solve E [] = [] |
    solve E ((VAR x, CONST k)::S) = if(circ E) then (raise circularity ("Circularity in "^readSist(E)^"\n"))
                                    else if(constError E E) then (raise constantError ("Matching of different costants in "^readSist(E)^"\n"))
                                    else ((x, NUM k)::(solve E (substitute S (VAR x, CONST k))))|
    solve E ((VAR x, VAR y)::S) = if(circ E) then (raise circularity ("Circularity in "^readSist(E)^"\n"))
                                  else if(constError E E) then (raise constantError ("Matching of different costants in "^readSist(E)^"\n"))
                                  else (solve E (S@[(VAR x, VAR y)])) |
    solve E ((VAR x, PAIR(u,v))::S) = if(circ E) then (raise circularity ("Circularity in "^readSist(E)^"\n"))
                                      else if(constError E E) then (raise constantError ("Matching of different costants in "^readSist(E)^"\n"))
                                      else if(constPair (PAIR(u,v))) then (x, patToVal(PAIR (u,v)))::(solve ((VAR x, PAIR(u,v))::E) (substitute S (VAR x, PAIR(u,v))))
                                      else solve E (S@[((VAR x, PAIR(u,v)))]) |
    solve E ((CONST k, VAR x)::S) = if(circ E) then (raise circularity ("Circularity in "^readSist(E)^"\n"))
                                    else if(constError E E) then (raise constantError ("Matching of different costants in "^readSist(E)^"\n"))
                                    else ((x, NUM k)::(solve E (substitute S (VAR x, CONST k)))) |
    solve E ((CONST k, CONST h)::S) = if(circ E) then (raise circularity ("Circularity in "^readSist(E)^"\n"))
                                      else if(constError E E) then (raise constantError ("Matching of different costants in "^readSist(E)^"\n"))
                                      else (solve E S) |
    solve E ((PAIR(u,v), PAIR(x,y))::S) = if(circ E) then (raise circularity ("Circularity in "^readSist(E)^"\n"))
                                          else if(constError E E) then (raise constantError ("Matching of different costants in "^readSist(E)^"\n"))
                                          else (solve E ((sist u x)@(sist v y)@S) ) |
    solve E ((PAIR(u,v), VAR x)::S) = (solve E (( VAR x,PAIR(u,v))::S));

(* Given a system S, solve S after removing its duplicates (if any). *)
fun solver S = removeDup(solve S S)
                    handle circularity msg => if(print(msg)=()) then ([]) else ([]) |
                           constantError msg => if(print(msg)=()) then ([]) else ([]);

(* Given an environment E and an expression "let P = Q in ...", return Q' = E_minus_delta(P)(Q). *)
fun newPat E p q = patternSub (redEnv E (delta p)) q;

(* Given an environment E and an expression "let P = Q in ...", compute an environemnt E' by solving the system P = Q'. *)
fun newEnv E p q = solver (sist p (newPat E p q))
    handle error msg => if (print(msg)=()) then ([]) else ([]);

(* Given an environment E and an expression "let P = Q in ...", check whether delta(P) = dom(E'). *)
fun sideCond1 E p q =  (equalSet (delta p) (domEnv  (newEnv E p q)));

(* Given an environment E and an expression "let P = Q in ...", check whether E'(P) = E'(Q'). *)
fun sideCond3 E p q = let val E1 = (newEnv E p q) in (patternSub E1 p)=(patternSub E1 (newPat E p q)) end;

exception cannotLet of string;

(*
 * Implement the following evaluation function:
 * eval : Env x Exp --> Val
 * Side conditions:
 * 1. delta(P) = dom(E')		==> sideCond1 function
 * 2. Q' = E_meno_delta(P)(Q)	==> always true by construction of Q'
 * 3. E'(P) = E'(Q')			==> sideCond3 function
 *)
fun evall E (NUMERAL k) = (NUM k) |
    evall E (VARR x) =  (find x E) |
    evall E (DUO (x,y)) = (COUPLE((evall E x), (evall E y))) |
    evall E (LET (p,q,exp)) = if (not(sideCond1 E p q)) then (raise cannotLet ("Error: delta("^readPat(p)^") != dom( "^readEnv((newEnv E p q))^")\n"))
                              else if (not(sideCond3 E p q)) then (raise cannotLet ("Error: "^readPat(patternSub (newEnv E p q) p) ^ " != " ^ readPat(patternSub (newEnv E p q) (newPat E p q)) ^ "\n"))
                              else (evall ((newEnv E p q)@E) exp);

(* Wrapper for the evall function, handling exceptions (if any). *)
fun eval E exp = evall E exp
    handle cannotLet msg => if (print(msg)=()) then (NUM ~1) else (NUM ~1) |
           noSuchVariable msg => if (print(msg)=()) then (NUM ~1) else (NUM ~1);

(* TEST VALUES *)

val p1 = PAIR(VAR "x", PAIR(CONST 2, CONST 3));
val q1 = PAIR(CONST 5, PAIR(VAR "y", VAR "z"));
val E1 = [("y", NUM 2), ("z", NUM 3)];
val l1 = LET (p1, q1, VARR "x");

val S1 = sist p1 q1;
val S2 = [(VAR "y", CONST 7), (VAR "x", CONST 7), (VAR "y", VAR "x")];
val S3 = [(VAR "y", CONST 7), (VAR "x", VAR "y"), (VAR "x", CONST 4)]; (* Error checking example: it doesn't know that x is already defined. *)
val S4 = [(VAR "x", VAR "y"), (VAR "y", VAR "j"), (VAR "j", VAR "x")];
val S5 = [(VAR "x", PAIR(VAR "y", CONST 3)),(VAR "w",CONST 3)];
val S6 = [(PAIR (VAR "x", VAR "y"), PAIR (VAR "y", VAR "x"))];
val S7 = [(VAR "x", VAR "z"), (VAR "y", CONST 3), (VAR "z", PAIR(CONST 3, CONST 4))];
val S8 = [(VAR "y", VAR "z"),(VAR "z", CONST 3)];

val a = PAIR(VAR "x", VAR "x");
val b = PAIR(CONST 4, CONST 5);

val S = [(VAR "a", PAIR(VAR "a", VAR "b")), (VAR "d", PAIR(VAR "e", VAR "c")), (VAR "c", CONST 7)];

val pp = PAIR(CONST 5, (PAIR(VAR "x", VAR "y")));
val qq = PAIR (VAR "y", PAIR ( PAIR (VAR "y", VAR "z"), VAR "z")); 

val ppp = PAIR (PAIR (VAR "x", VAR "y"), PAIR (VAR "w", VAR "z"));
val qqq = PAIR( PAIR (CONST 3, CONST 4), PAIR (VAR "y", PAIR (VAR "x", VAR "y")));

val p2 = PAIR( VAR "x", PAIR( PAIR (VAR "y", VAR "z" ), VAR "w" ) );
val q2 = PAIR( PAIR( PAIR ( PAIR (CONST 3, CONST 2), CONST 9), CONST 6), VAR "x");

val p3 = PAIR(VAR "x", CONST 5);
val q3 = PAIR(CONST 2, PAIR (CONST 3, CONST 4));

val l = DUO (LET (VAR "x", CONST 3, LET (PAIR (VAR "y", PAIR (CONST 7, VAR "z")), PAIR (VAR "z", PAIR (CONST 7, VAR "x")), VARR "y")), NUMERAL 5);

val p4 = PAIR (VAR "y", PAIR (CONST 7, VAR "z"));
val q4 = PAIR (VAR "z", PAIR (CONST 7, VAR "x"));

val l2 = LET (PAIR (VAR "y", PAIR (CONST 7, VAR "z")), PAIR (VAR "z", PAIR (CONST 7, CONST 3)), VARR "y");

val e = [];
val p = PAIR (VAR "y", PAIR (CONST 7, VAR "z"));
val q = PAIR (VAR "z", PAIR (CONST 7, CONST 3));




