Pattern Matching Expressions
=============================

This project is a Standard ML implementation of an interpreter for language of pattern matching expressions.

For more informations check the official page: http://aelog.org/pattern-matching-expressions
